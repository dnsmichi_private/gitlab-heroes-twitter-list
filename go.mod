module gitlab-heroes-twitter-list

go 1.12

require (
	github.com/PuerkitoBio/goquery v1.5.0
	github.com/dghubble/go-twitter v0.0.0-20190719072343-39e5462e111f
	github.com/dghubble/oauth1 v0.6.0
	golang.org/x/net v0.0.0-20200114155413-6afb5195e5aa // indirect
)
